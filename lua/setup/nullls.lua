local null_ls = require('null-ls')

null_ls.config({
  sources = {
    null_ls.builtins.diagnostics.eslint, -- eslint or eslint_d
    null_ls.builtins.code_actions.eslint, -- eslint or eslint_d
    null_ls.builtins.formatting.prettier -- prettier, eslint, eslin
  }
})

require('lspconfig')['null-ls'].setup({})
