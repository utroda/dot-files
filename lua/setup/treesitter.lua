local ts = require('nvim-treesitter.configs')

ts.setup({
  ensure_installed = 'maintained',
  highlight = {
    enabled = true,
  },
  autotag = {
    enable = true,
  }
})
