local bl = require('indent_blankline')

bl.setup({
  space_char_blankline = " ",
  show_current_context = true,
  show_current_context_start = true,
})
