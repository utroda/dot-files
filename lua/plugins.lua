vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

local fn = vim.fn
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  })
end
vim.api.nvim_command("packadd packer.nvim")
-- returns the require for use in `config` parameter of packer's use
-- expects the name of the config file
function get_setup(name)
  return string.format('require("setup/%s")', name)
end

return require('packer').startup({
  function(use)
    use('wbthomason/packer.nvim')
    use('kyazdani42/nvim-web-devicons')
    use({'gruvbox-community/gruvbox', 
      config = get_setup('colortheme')
    })
    --use({'rose-pine/neovim',
    --  as = 'rose-pine',
    --  config = get_setup('rosepine'),
    --})
    use({ 'nvim-telescope/telescope.nvim',
      requires = {
        { 'nvim-lua/plenary.nvim' },
        { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
      },
      config = get_setup('telescope')
    })
    use({ 'nvim-treesitter/nvim-treesitter',
      run = ':TSUpdate',
      config = get_setup('treesitter')
    })
    use({ 'neovim/nvim-lspconfig', 
      requires = {
        { 'jose-elias-alvarez/nvim-lsp-ts-utils' },
        { 'jose-elias-alvarez/null-ls.nvim', config = get_setup('nullls') },
      },
      config = get_setup('lsp')
    })
    use({ 'hrsh7th/nvim-cmp',
      requires = {
        { 'hrsh7th/cmp-nvim-lsp' },
        { 'hrsh7th/cmp-buffer' },
        { 'hrsh7th/cmp-path' },
        { 'hrsh7th/cmp-cmdline' },
        { 'hrsh7th/cmp-vsnip' },
        { 'hrsh7th/vim-vsnip' },
      },
      config = get_setup('cmp'),
    })
    use({'windwp/nvim-ts-autotag'})
    use({ 'windwp/nvim-autopairs',
      after = { 'nvim-cmp' },
      config = get_setup('autopairs')
    })
    use({ 'goolord/alpha-nvim',
      config = get_setup('alpha')
    })
    use({ 'folke/todo-comments.nvim', 
      requires = {
        { 'nvim-lua/plenary.nvim' },
      },
      config = get_setup('comments'),
    })
    use({ 'lukas-reineke/indent-blankline.nvim',
      config = get_setup('blankline')
    })
    use({ 'kkoomen/vim-doge' })
  end,
  config = {
    display = {
      open_fn = require('packer.util').float,
    },
    profile = {
      enable = true,
      threshold = 1,
    },
  },
})
