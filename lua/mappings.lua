local function map(mode, lhs, rhs, opts)
  local options = { noremap = true }
  if opts then
    options = vim.tbl_extend("force", options, opts)
  end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

vim.g.mapleader = " "

map('i', 'jj', '<Esc>')
map('n', '<leader>t', '<C-^>')
map('n', '<C-p>', '<cmd>Telescope find_files<cr>')
map('n', '<C-t>', '<cmd>Telescope treesitter<cr>')
map('n', '<leader>fg', '<cmd>Telescope live_grep<cr>')
map('n', '<leader>fc', '<cmd>Telescope git_commits<cr>')
map('n', '<leader>fb', '<cmd>Telescope git_branches<cr>')
map('n', '<leader>ft', '<cmd>TodoTelescope<cr>')
map('n', 'Y', 'y$')

-- keep in center of screen
map('n', 'n', 'nzzzv')
map('n', 'N', 'Nzzzv')
map('n', 'J', 'mzJ`v')
