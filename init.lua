-- Created By Jared West - Repo https://github.com/utroda/dotfiles
-- You will need to install eslint_d `npm install -g eslint_d`
-- You will need to install language servers `npm i -g vscode-langservers-extracted` and `npm install -g typescript typescript-language-server`
require("plugins")
require("options")
require("mappings")
